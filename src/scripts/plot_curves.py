import argparse
import pickle
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, auc
from utils import get_data


matplotlib.rcParams.update({'font.size': 14})

COLORS = ["#b30000", "#377eb8","#4daf4a", "#ff67a4", "#984ea3","#ff7f00"]
COLORS2= ["#b30000", "#377eb8","#984ea3","#ff7f00", "#4daf4a", "#ff67a4"]

def plot_roc(results, fname, title=None):
    plt.rc('axes', color_cycle=COLORS)
    plt.clf()

    fig = plt.figure()
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * .8])

    for name in ["M-CAP", "MetaLR", "Eigen", "SIFT", "PolyPhen-2", "CADD"]:
        mean_tpr = results[name]['AUC']['mean_tpr']
        mean_fpr = results[name]['AUC']['mean_fpr']
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)

        label = '{} ({:.3f})'.format(name, mean_auc)
        ax.plot(mean_fpr, mean_tpr, '-', label=label, lw=1)

    label = 'Random'
    ax.plot([0, 1], [0, 1], '-', label=label, color=(0.6, 0.6, 0.6))

    #ax.plot([.98, .98], [1, 1], '--', color=(0.6,0.6,0.6))
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('Benign variants misclassified\n(False Positive Rate)')
    plt.ylabel('Pathogenic variants correctly classified\n(True Positive Rate)')
    if title:
        plt.title(title)

    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    
    plt.legend(loc='upper center', ncol=1, fontsize='x-small', bbox_to_anchor=(0.73,.525), title="Method (AUC)", frameon=False)
    plt.savefig(fname)

def plot_zoom_roc(results, fname, title=None):
    plt.rc('axes', color_cycle=COLORS2)
    plt.clf()

    fig = plt.figure()
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * .8])

    #ax.fill_between([0, 1], .95, 1, facecolor='lightgray')
    for name in ["M-CAP", "MetaLR", "PolyPhen-2", "CADD", "Eigen", "SIFT"]:
        mean_tpr = results[name]['AUC']['mean_tpr']
        mean_fpr = results[name]['AUC']['mean_fpr']
        mean_tpr[-1] = 1.0
        
        normalized_tpr = mean_tpr - .95
        normalized_tpr[np.where(normalized_tpr < 0)[0]] = 0.0
        normalized_auc = auc(mean_fpr, normalized_tpr) / 0.05

        label = '{} ({:.3f})'.format(name, normalized_auc)
        ax.plot(mean_fpr, mean_tpr, '-', label=label, lw=1)

    label = 'Random'
    ax.plot([0, 1], [0, 1], '-', label=label, color=(0.6, 0.6, 0.6))
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    plt.xlim([-0.05, 1.05])
    plt.ylim([0.90, 1.01])
    plt.xlabel('Benign variants misclassified\n(False Positive Rate)')
    plt.ylabel('Pathogenic variants correctly classified\n(True Positive Rate)')
    
    if title:
        plt.title(title)

    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)
    plt.legend(loc='upper center', ncol=1, fontsize='xx-small', bbox_to_anchor=(0.2,0.425), title="Method (hsr-AUC)", frameon=False)
    plt.savefig(fname)

def plot_fnr_fpr(results, fname, title=None):
    plt.clf()
    fig = plt.figure()
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * .8])

    for name in results:
        tnr = results[name]['FNR']['mean_tnr']
        fnr = results[name]['FNR']['mean_fnr']
        idx = np.argmin(np.absolute(fnr - .02))
        label = '{0} ({1:.2%} TNR at {2:.2%} FNR)'.format(name, tnr[idx], fnr[idx])
        ax.plot(fnr, tnr, '-', label=label, lw=1)

    plt.xlim([0.0, 0.05])
    plt.ylim([0.0, 0.8])
    plt.xlabel('Pathogenic variants misclassified\n(False Negative Rate)')
    plt.ylabel('Benign variants correctly classified\n(True Negative Rate)')
    if title:
        plt.title(title)

    plt.legend(loc='upper center', ncol=1, fontsize='xx-small', bbox_to_anchor=(0.5,-0.15), frameon=False)
    plt.savefig(fname)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('results', help='Name of pickled results file')
    ap.add_argument('--roc', help='Name of file for ROC curve', default='roc.png')
    ap.add_argument('--fnr', help='Name of file for FNR curve', default='fnr.png')
    args = ap.parse_args()

    with open(args.results, 'rb') as f:
        results = pickle.load(f)

    print(results.keys())
    plot_roc(results, args.roc)
    plot_zoom_roc(results, 'zoom' + args.roc)
    plot_fnr_fpr(results, args.fnr)
