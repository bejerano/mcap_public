set -x 

results_fname=$1
n_estimators=$2
lr=$3
subsample=$4
max_depth=$5
max_feature=$6
min_leafs=$7

user=kjag
script=../models/grad_boosting.py
PYTHONPATH=/cluster/u/varunbindra/newMCAP/mcap_public/src/scripts

python3 -u $script --out $results_fname --n-estimators $n_estimators --lr $lr \
	--subsample $subsample --max-depth $max_depth --max-features $max_feature \
	--min-leaves $min_leafs --with-metrics --save-clf
