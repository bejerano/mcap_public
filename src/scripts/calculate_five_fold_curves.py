import argparse
import getpass
import numpy as np
import pickle
import sklearn

from scipy import interp
from sklearn import linear_model
from sklearn.cross_validation import StratifiedKFold
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import roc_curve, auc
from sklearn.metrics.ranking import _binary_clf_curve
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC


NUM_FOLDS = 5

def calculate_auc_from_scores(X, Y):
    fpr, tpr, threshold = roc_curve(Y, X)
    fpr[0] = 0.0
    return [tpr, fpr]

def calculate_tnr_fnr(X, Y):
    fps, tps, thresholds = _binary_clf_curve(Y, X)
    fns = tps[-1] - tps
    tns = fps[-1] - fps

    tnr = tns / (tns + fps + 1e-12)
    tnr = tnr[::-1]

    fnr = fns / (tps + fns + 1e-12)
    fnr = fnr[::-1]

    return [tnr, fnr]

def calculate_mean_results(models):
    X, Y = get_dataset()

    results = {}
    for model in models:
        name = '{}'.format(model[0])
        results[name] = {}
        results[name]['clf'] = sklearn.base.clone(model[1])
        results[name]['AUC'] = {}
        results[name]['AUC']['mean_tpr'] = 0.0
        results[name]['AUC']['mean_fpr'] = np.linspace(0, 1, 100)
        results[name]['FNR'] = {}
        results[name]['FNR']['mean_tnr'] = 0.0
        results[name]['FNR']['mean_fnr'] = np.linspace(0, 1, 100)
        results[name]['X'] = X
        results[name]['Y'] = Y

    for train, test in StratifiedKFold(Y, n_folds=NUM_FOLDS, shuffle=True):
        for model_name in results:
            clf = results[model_name]['clf']
            X, Y = results[model_name]['X'], results[model_name]['Y']
            probs = clf.fit(X[train], Y[train]).predict_proba(X[test])
            fpr, tpr, thresholds = roc_curve(Y[test], probs[:,1])
            mean_tpr = results[model_name]['AUC']['mean_tpr']
            mean_fpr = results[model_name]['AUC']['mean_fpr']
            mean_tpr += interp(mean_fpr, fpr, tpr)
            mean_tpr[0] = 0.0
            results[model_name]['AUC']['mean_tpr'] = mean_tpr
            results[model_name]['AUC']['mean_fpr'] = mean_fpr

            tnr, fnr = calculate_tnr_fnr(probs[:,1], Y[test])
            mean_tnr = results[model_name]['FNR']['mean_tnr']
            mean_fnr = results[model_name]['FNR']['mean_fnr']
            mean_tnr += interp(mean_fnr, fnr, tnr)
            mean_tnr[0] = 0.0
            results[model_name]['FNR']['mean_tnr'] = mean_tnr
            results[model_name]['FNR']['mean_fnr'] = mean_fnr

    for model_name in results:
        results[model_name]['AUC']['mean_tpr'] /= NUM_FOLDS
        results[model_name]['FNR']['mean_tnr'] /= NUM_FOLDS

    return results


def calculate_five_fold_results(dest_fname, pathogenic_fname, benign_fname):
    models = []
    grad_boost = GradientBoostingClassifier(
        n_estimators=3750,
        learning_rate=.05,
        subsample=1.0,
        max_depth=6,
        max_features=.3,
        min_samples_leaf=3,
    )
    models.append(('Gradient Boosting', grad_boost))

    results = calculate_mean_results(models)

    scores = [
        ('CADD', cadd_scores),
        ('PolyPhen-2', polyphen2_scores),
        ('SIFT', sift_scores),
        ('dbNSFP Ensemble SVM', dbnsfp_svm_scores),
        ('dbNSFP Ensemble LR', dbnsfp_lr_scores),
    ]

    for name, scores_func in scores:
        X, Y = scores_func(pathogenic_fname, benign_fname)
        tpr, fpr = calculate_auc_from_scores(X, Y)
        results[name] = {}
        results[name]['AUC'] = {}
        results[name]['AUC']['mean_tpr'] = tpr
        results[name]['AUC']['mean_fpr'] = fpr

        tnr, fnr = calculate_tnr_fnr(X, Y)
        results[name]['FNR'] = {}
        results[name]['FNR']['mean_tnr'] = tnr
        results[name]['FNR']['mean_fnr'] = fnr

    with open(dest_fname, 'wb') as f:
        pickle.dump(results, f)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('output', help='Name of file to store pickled results')
    args = ap.parse_args()

    calculate_five_fold_results(args.output, args.pathogenic_filename, args.benign_filename)
