import argparse
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import glob
import os
import math
import random
from pylab import plot, show, savefig, xlim, figure, hold, ylim, legend, boxplot, setp, axes, ylabel
from scipy.stats import mannwhitneyu, ks_2samp
from collections import namedtuple, defaultdict
from sklearn.externals import joblib

from evaluate_test_set import get_dataset
from evaluate_test_set import cadd_scores
from evaluate_test_set import polyphen2_scores
from evaluate_test_set import sift_scores
from evaluate_test_set import dbnsfp_lr_scores
from evaluate_test_set import eigen_scores

SENSITIVITY = .95
POINT_SIZE = 70

def kept_variant(patient_data, patient_id=None, method=None, all_scores=None):
    kept = False
    causal_variants = [10544634,10544635,118348811,686925,163133342,65978677,153586596, 31778920, 67592129, 56385308, 43555323, 121768709, 2548334]
    for snp in patient_data:
        snp_scores = snp
        snp = (snp[0],int(snp[1]))
        if snp[1] in causal_variants:
            kept = True
            if method=="MCAP":
                for score in all_scores:
                    if score[1]==snp[1]:
                        break
                print("CHR:%f\tPOS:%f\tSIFT:%f\tEigen:%f\tPolyPhen-2:%f\tCADD:%f\tMetaLR:%f\tM-CAP:%f"%(snp_scores[0],snp_scores[1], score[-6], score[-1], score[-4], score[-5], score[-2], snp_scores[4]))
            break
    return 1 if kept else 0

def read_patients_orig_methods(dir_name, thresholds):
    patients = []
    Patient = namedtuple('Patient', ['ID', 'Raw', 'Total','Total_KEEPS_SNP','SIFT','SIFT_KEEPS_SNP','CADD','CADD_KEEPS_SNP','PolyPhen2','PolyPhen2_KEEPS_SNP','MetaLR', 'MetaLR_KEEPS_SNP', 'Eigen', 'Eigen_KEEPS_SNP'])
    patient_files = glob.glob(os.path.join(dir_name, '*.txt'))
    for patient_file in patient_files:
        patient_id = patient_file.split("/")[-1].split(".")[0]
        if patient_id=='JB3-a1':
            continue

        patient_data = np.genfromtxt(patient_file, delimiter='\t',skip_header=1)
        patient_total = len(patient_data)
        patient = Patient(
            patient_id,
            patient_total,
            100*patient_total/patient_total, 2, 
            100*len(patient_data[patient_data[:,-6] < thresholds[0]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-6] < thresholds[0]]), 
            100*len(patient_data[patient_data[:,-5] >= thresholds[1]])/patient_total,
            kept_variant(patient_data[patient_data[:,-5] >= thresholds[1]]), 
            100*len(patient_data[patient_data[:,-4] > thresholds[2]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-4] > thresholds[2]]), 
            100*len(patient_data[patient_data[:,-2] > thresholds[3]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-2] > thresholds[3]]),
            100*len(patient_data[patient_data[:,-1] > thresholds[4]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-1] > thresholds[4]])
            )
        print (patient)
        patients.append(patient)
    return patients

def read_patients_new_methods(dir_name, thresholds):
    patients = []
    Patient = namedtuple('Patient', ['ID', 'Raw', 'Total','Total_KEEPS_SNP','SIFT','SIFT_KEEPS_SNP','CADD','CADD_KEEPS_SNP','PolyPhen2','PolyPhen2_KEEPS_SNP', 'MetaLR', 'MetaLR_KEEPS_SNP', 'Eigen', 'Eigen_KEEPS_SNP', 'MCAP', 'MCAP_KEEPS_SNP'])
    patient_files = glob.glob(os.path.join(dir_name, '*.txt'))
    i = 0
    totals = defaultdict(int)
    for patient_file in patient_files:
        if "_results.txt" in patient_file.split("/")[-1] or ".txt" not in patient_file.split("/")[-1]:
            continue

        patient_id = patient_file.split("/")[-1].split(".")[0]
        if patient_id=='JB3-a1':
            continue

        evaluated_file = os.path.join(dir_name, patient_id+"_results.txt")
        evaluated_data = np.genfromtxt(evaluated_file, delimiter='\t',skip_header=1)
        patient_data = np.genfromtxt(patient_file, delimiter='\t',skip_header=1)

        # for patient how many variants are kept and is the causal variant kept ?
        #print(evaluated_file, patient_id, len(evaluated_data[evaluated_data[:,4] > thresholds[5]]), kept_variant(evaluated_data[evaluated_data[:,4] > thresholds[5]]))
        patient_total = len(patient_data)

        patient = Patient(
            patient_id, patient_total, 100*patient_total/patient_total, 2, 
            100*len(patient_data[patient_data[:,-6] < thresholds[0]])/patient_total,
            kept_variant(patient_data[patient_data[:,-6] < thresholds[0]]),
            100*len(patient_data[patient_data[:,-5] > thresholds[1]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-5] > thresholds[1]]), 
            100*len(patient_data[patient_data[:,-4] > thresholds[2]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-4] > thresholds[2]]), 
            100*len(patient_data[patient_data[:,-2] > thresholds[3]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-2] > thresholds[3]]), 
            100*len(patient_data[patient_data[:,-1] > thresholds[4]])/patient_total, 
            kept_variant(patient_data[patient_data[:,-1] > thresholds[4]]), 
            100*len(evaluated_data[evaluated_data[:,4] > thresholds[5]])/patient_total, 
            kept_variant(evaluated_data[evaluated_data[:,4] > thresholds[5]], patient_id=patient_id, method="MCAP", all_scores=patient_data))
        i = i + 1
        #patient = Patient(patient_id, len(patient_data), len(patient_data[patient_data[:,-5] >= 0]), 0, len(patient_data[patient_data[:,-4] >= 2]), 0, len(patient_data[patient_data[:,-3] <= 1]), 0)
        patients.append(patient)
        totals["TOTAL"] += patient_total
        totals["SIFT"] += len(patient_data[patient_data[:,-6] < thresholds[0]])
        totals["CADD"] += len(patient_data[patient_data[:,-5] > thresholds[1]])
        totals["PolyPhen-2"] += len(patient_data[patient_data[:,-4] > thresholds[2]])
        totals["MetaLR"] += len(patient_data[patient_data[:,-2] > thresholds[3]])
        totals["EIGEN"] += len(patient_data[patient_data[:,-1] > thresholds[4]])
        totals["M-CAP"] += len(evaluated_data[evaluated_data[:,4] > thresholds[5]])
    return totals, patients

def get_color(field):
    if int(field) == 1:
        return 'black', '.'
    if int(field) == 2:
        return 'black', '.'
    else:
        return 'r', 'x'

def plot_boxplot(patients, out_fname, methods, labels, title):
    fig = figure(figsize = (12,10))
    ax = axes()
    box = ax.get_position()
    ax.set_position([box.x0 , box.y0 + .1*box.height, box.width*.8, box.height*.8])
    ax.set_title(title)
    hold(True)

    position = 1
    all_data = []
    for method in methods[1:]:
        method_data = []
        for patient in patients:
            method_data.append(int(getattr(patient, method)))
        all_data.append(method_data)
    bp = boxplot(all_data)
    print("\t".join([str(data) for data in all_data[3]]), file=open("metalr_dropped.txt", "w"))
    print("\t".join([str(data) for data in all_data[4]]), file=open("mcap_dropped.txt", "w"))
    print('elr,yams', ks_2samp(all_data[3],all_data[4]))
    # set axes limits and labels
    ylim(0,105)

    ax.set_xticklabels(labels[1:])
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ylabel('% of rare candidate variants \n classified as possibly pathogenic', fontsize=18)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    savefig('box_patients'+out_fname)
    
def plot_results(patients, out_fname, methods, legend_coords, plot_title=""):
    fig, ax = plt.subplots()
    points = []
    final_results = {"num_candidates":defaultdict(list),"predict":defaultdict(list)}

    for i, patient in enumerate(patients):
        patient_vals = []
        for j, method in enumerate(methods):
            # plot each patient and method
            color, mark = get_color(getattr(patient, method + '_KEEPS_SNP'))
            points.append(ax.scatter(j, int(getattr(patient, method)), s=POINT_SIZE, c=color, marker=mark))
            patient_vals.append(int(getattr(patient, method)))

            # aggregate the relevant data for new plots
            final_results["num_candidates"][method].append(getattr(patient, method))
            final_results["predict"][method].append(mark)

    plt.plot(range(0,len(patient_vals)), patient_vals,'k-')
    drop_counts = defaultdict(list)

    # how many of the causal variants are misclassified
    for k,v in final_results["predict"].items():
        print(k, len([p for p in v if p=="x"]))
    
    for k,v in final_results["num_candidates"].items():
        v.sort()
    
    #print final_results
    plt.title(plot_title, y=1.05)
    plt.ylabel('% of Candidate SNVs')
    plt.ylim([0,105])
    plt.xlim([-1, len(methods)])
    ax.set_xticks(range(len(methods)))
    scatter_labels = []
    for method in methods:
        if method == "Total":
            misclassify_label = "misclassified:"
        else:
            misclassify_label = str(len([pred for pred in final_results["predict"][method] if pred=="x"]))
            if method == 'Eigen':
                misclassify_label = str(int(misclassify_label) - 1)

        scatter_labels.append(method + "\n\n" + misclassify_label)

    ax.set_xticklabels(scatter_labels)

    hR, = plt.plot([1,1],'rx')
    plt.legend((hR,),('Causative variant \n misclassified',), bbox_to_anchor=legend_coords, numpoints=1)
    hR.set_visible(False)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    plt.savefig(out_fname)
    plt.clf()
    return scatter_labels

def determine_threshold(scores):
    start, stop = np.min(scores), np.max(scores)
    best_threshold = None
    best_percent = 1
    thresholds = np.linspace(start, stop, num=10000)

    for t in thresholds:
        percent = float(np.sum(scores > t)) / scores.shape[0]
        if percent >= SENSITIVITY and (percent < best_percent):
            best_percent = percent
            best_threshold = t
    print (best_threshold, best_percent)
    return best_threshold

def calculate_sensitive_thresholds(clf_fname):
    methods = [sift_scores, cadd_scores, polyphen2_scores, dbnsfp_lr_scores, eigen_scores]
    sensitive_thresholds = []
    
    for method in methods:
        x, y = method()
        scores = x[np.where(y == 1)]
        best_threshold = determine_threshold(scores)
        sensitive_thresholds.append(best_threshold)
    clf = joblib.load(clf_fname)
    X, Y = get_dataset(True)
    X = X[np.where(Y == 1)[0]]
    scores = clf.predict_proba(X)[:,1]
    best_threshold = determine_threshold(scores)
    sensitive_thresholds.append(best_threshold)

    sensitive_thresholds[0] *= -1
    return sensitive_thresholds

def compute_stats(patients, methods, datasets, default_thresholds):

    # fraction of pathogenic misclassified at default thresholds
    print ("fraction of pathogenic misclassified at default thresholds")
    for data,threshold in zip(datasets, default_thresholds):
        x, y = data()
        scores = x[np.where(y == 1)]
        percent = np.sum(scores < threshold)/scores.shape[0]
        print (data,percent)

    stats = defaultdict(list)
    methods.append('Raw')
    for patient in patients:
        for method in methods:
            stats[method].append(getattr(patient, method))

    print("Mean % of variants dropped")
    for method in methods:
        if method == "Raw":
            print(method, np.mean(stats[method]))
        else:
            print(method, 100 - np.mean(stats[method]))
        
    if "MCAP" in methods:
        # compute the statistical test to determine how different the distributions are.
        for method in methods:
            print('%s,MCAP'%method, ks_2samp(stats[method],stats['MCAP']))

def autolabel(rects, ax):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height) + '%',
                ha='center', va='bottom')

def plot_bars(counts, filename, labels):
    fig, ax = plt.subplots()
    methods = ['SIFT','EIGEN','PolyPhen-2', 'CADD','MetaLR','M-CAP']
    vals = [round(100 - ((100.0*counts[method])/counts["TOTAL"])) for method in methods]
    print (vals)
    rects = ax.bar(np.arange(len(vals)) + .5, vals, .75, color='blue')
    ax.set_xticks(np.arange(len(methods)) + 7/8)
    ax.set_ylim(0,100)
    ax.set_xticklabels(methods)
    ax.set_title("% of total variants called benign with high confidence")
    ax.set_ylabel("% of variants")

    autolabel(rects, ax)
    plt.savefig(filename)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('results_dir', help='Directory with patient results')
    ap.add_argument('clf', help='Saved clf model')
    ap.add_argument('out', help='Name of output graph')
    args = ap.parse_args()
    
    #thresholds = calculate_sensitive_thresholds(args.clf)
    #print("Statistics for high sensitivity thresholds", thresholds)
    totals, sensitivity_95_patients = read_patients_new_methods(args.results_dir, [0.49, 10.34, 0.022, 0.105, 1.00, 0.025])
    methods = ['Total', 'SIFT', 'PolyPhen2', 'CADD', 'Eigen', 'MetaLR', 'MCAP']
    scatter_labels = plot_results(sensitivity_95_patients, "sensitivity_95_" + args.out, methods, (.57,.2), plot_title='95% Sensitivity Thresholds')
    plot_boxplot(sensitivity_95_patients, "patients_95" + args.out, methods, scatter_labels, title="95% Sensitivity Threshold")
    compute_stats(sensitivity_95_patients, methods, [sift_scores, cadd_scores, polyphen2_scores, dbnsfp_lr_scores], [-0.05, 20, .8, .5])
    plot_bars(totals, "bar_95" + args.out, scatter_labels)
