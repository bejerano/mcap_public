import argparse
import getpass
import glob
import numpy as np
import os

from sklearn.externals import joblib

#global variables
THRESHOLD = .025
CADD_INDEX = -5
DBNSFP_SVM_INDEX = -3
DBNSFP_LR_INDEX = -2
POLYPHEN2_INDEX = -4
SIFT_INDEX = -6
SIPHY_INDEX = -7
MUTATION_ASSESSOR_INDEX = -8
MUTATION_TASTER_INDEX = -9
LRT_INDEX = -10
GERP_INDEX = -11
FATHMM_INDEX = -12

def get_data(fname):
    data = np.genfromtxt(fname, delimiter="\t", dtype=np.dtype(str), skip_header=1)
    return data

def get_scores(index, fname):
    data = get_data(fname)
    X = data[:, index]
    return X

def cadd_scores(fname):
    X = get_scores(CADD_INDEX, fname=fname)
    return X.astype(float)

def polyphen2_scores(fname):
    X = get_scores(POLYPHEN2_INDEX, fname=fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def sift_scores(fname):
    X = get_scores(SIFT_INDEX,fname)
    X[np.where(X == '')] = 0
    return -X.astype(float)

def dbnsfp_svm_scores(fname):
    X = get_scores(DBNSFP_SVM_INDEX, fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def dbnsfp_lr_scores(fname):
    X = get_scores(DBNSFP_LR_INDEX, fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def fathmm_scores(fname):
    X = get_scores(FATHMM_INDEX, fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def gerp_scores(fname):
    X = get_scores(GERP_INDEX, fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def lrt_scores(fname):
    X = get_scores(LRT_INDEX, fname=fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def mutation_taster_scores(fname):
    X = get_scores(MUTATION_TASTER_INDEX, fname=fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def mutation_assessor_scores(fname):
    X = get_scores(MUTATION_ASSESSOR_INDEX, fname=fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def siphy_scores(fname):
    X = get_scores(SIPHY_INDEX, fname=fname)
    X[np.where(X == '')] = 1
    return X.astype(float)

def load_features(fname):
    features = np.genfromtxt(fname, delimiter="\t", dtype=np.dtype(str), skip_header=1)
    if len(features)==0:
        return None, None
    snp_info = features[:,:4]
    features = features[:,4:-12].astype(float)

    #features = features.astype(float)

    cadd = cadd_scores(fname)
    sift = sift_scores(fname)
    polyphen2 = polyphen2_scores(fname)
    svm = dbnsfp_svm_scores(fname)
    lr = dbnsfp_lr_scores(fname)
    fathmm = fathmm_scores(fname)
    gerp = gerp_scores(fname)
    lrt = lrt_scores(fname)
    mutation_taster = mutation_taster_scores(fname)
    mutation_assessor = mutation_assessor_scores(fname)
    siphy = siphy_scores(fname)
    features = np.hstack((features, sift[:,None], gerp[:,None], lrt[:,None], mutation_assessor[:,None], siphy[:,None], cadd[:,None], polyphen2[:,None], fathmm[:,None], mutation_taster[:,None], svm[:,None], lr[:,None]))
    
    return snp_info, features

def write_results(results_fname, results):
    with open(results_fname, 'w') as f:
        header = ['CHROM', 'POS', 'REF', 'ALT', 'PROB_OF_HARMFUL', 'PREDICTION']
        f.write('\t'.join(header) + '\n')
        for row in results:
            line = '\t'.join(row) + '\n'
            f.write(line)

def evaluate_patient(clf, features_fname, results_fname):
    snp_info, features = load_features(features_fname)
    if snp_info is None:
        return
    probs = clf.predict_proba(features)
    preds = (probs > THRESHOLD).astype('float')[:,1]
    preds = np.where(preds == 1, 'Harmful', 'Harmless')
    str_probs = np.array([str(p) for p in probs[:,1]])[:,None]
    results = np.hstack((snp_info, str_probs, preds[:,None]))
    write_results(results_fname, results)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('clf', type=str, help='Classifer to evaluate SNPs')
    ap.add_argument('snps', type=str, help='SNPs features file')
    ap.add_argument('output', type=str, help='Output file for results')
    args = ap.parse_args()

    clf = joblib.load(args.clf)
    evaluate_patient(clf, args.snps, args.output)
