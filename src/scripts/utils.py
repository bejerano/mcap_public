import getpass
import numpy as np
import sklearn

from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import StratifiedKFold

NUM_FOLDS = 5   # Number of folds for cross validation
PATHOGENIC_THRESHOLD = .95
BENIGN_THRESHOLD = .95

PATHOGENIC_FEATURES="<INSERT FILEPATH TO PATHOGENIC TRAINING DATA>"
BENIGN_FEATURES="<INSERT FILEPATH TO BENIGN TRAINING DATA>"

# setting up the variables to know which index in the feature file corresponds to which feature
EIGEN_INDEX = -1
DBNSFP_LR_INDEX = -2
DBNSFP_SVM_INDEX = -3
POLYPHEN2_INDEX = -4
CADD_INDEX = -5
SIFT_INDEX = -6
SIPHY_INDEX = -7
MUTATION_ASSESSOR_INDEX = -8
MUTATION_TASTER_INDEX = -9
LRT_INDEX = -10
GERP_INDEX = -11
FATHMM_INDEX = -12


def get_data(pathogenic_fname=PATHOGENIC_FEATURES, benign_fname=BENIGN_FEATURES):
    pathogenic_data = np.genfromtxt(pathogenic_fname, delimiter="\t", dtype=np.dtype(str), skip_header=1)
    benign_data = np.genfromtxt(benign_fname, delimiter="\t", dtype=np.dtype(str), skip_header=1)
    return pathogenic_data, benign_data

def get_dataset(with_metrics, pathogenic_fname=PATHOGENIC_FEATURES, benign_fname=BENIGN_FEATURES):
    pathogenic, benign = get_data(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
    pathogenic = pathogenic[:,4:-12].astype(float)
    benign = benign[:,4:-12].astype(float)
    
    X = np.vstack((pathogenic, benign))
    Y = np.vstack((np.ones((len(pathogenic),1)), np.zeros((len(benign),1))))

    if with_metrics:
        cadd, _ = cadd_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        sift, _ = sift_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        polyphen2, _ = polyphen2_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        gerp, _ = gerp_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        lrt, _ = lrt_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        mutation_assessor, _ = mutation_assessor_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        siphy, _ = siphy_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)

        fathmm, _ = fathmm_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        mutation_taster, _ = mutation_taster_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        svm, _ = dbnsfp_svm_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        lr, _ = dbnsfp_lr_scores(pathogenic_fname=pathogenic_fname, benign_fname=benign_fname)
        X = np.hstack((X, sift, gerp, lrt, mutation_assessor, siphy, cadd, polyphen2, fathmm, mutation_taster, svm, lr))

    return X, Y

def get_scores(index, pathogenic_fname, benign_fname):
    pathogenic_data, benign_data = get_data(pathogenic_fname, benign_fname)
    pathogenic_data = pathogenic_data[:, index]
    benign_data = benign_data[:, index]

    X = np.vstack((pathogenic_data[:,None], benign_data[:,None]))
    Y = np.vstack((np.ones((len(pathogenic_data),1)), np.zeros((len(benign_data),1)))).T[0,:]
    return X, Y

def cadd_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(CADD_INDEX, pathogenic_fname, benign_fname)
    return X.astype(float), Y

def polyphen2_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(POLYPHEN2_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def sift_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(SIFT_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 0
    return -X.astype(float), Y

def dbnsfp_svm_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(DBNSFP_SVM_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def dbnsfp_lr_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(DBNSFP_LR_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def fathmm_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(FATHMM_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def gerp_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(GERP_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def lrt_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(LRT_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def mutation_taster_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(MUTATION_TASTER_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def mutation_assessor_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(MUTATION_ASSESSOR_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def siphy_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(SIPHY_INDEX, pathogenic_fname, benign_fname)
    X[np.where(X == '')] = 1
    return X.astype(float), Y

def eigen_scores(pathogenic_fname, benign_fname):
    X, Y = get_scores(EIGEN_INDEX, pathogenic_fname, benign_fname)
    # Subset to the SNPs that we have scores for
    X, Y = X[np.where(X != '')[0]], Y[np.where(X != '')[0]]
    return X.astype(float), Y

def get_feature_names():
    with open(PATHOGENIC_FEATURES) as f:
        header = f.readline().strip('\n')
        return header.split('\t')

def write_data(fname, data):
    np.savetxt(fname, data, delimiter='\t', fmt='%s')

def write_probs(clf_name, hgmd_data, kgp_data):
    start, end = PATHOGENIC_FEATURES.rsplit('.', 1)
    fname = '.'.join([start, '{}_probs'.format(clf_name), end])
    write_data(fname, hgmd_data)

    start, end = BENIGN_FEATURES.rsplit('.', 1)
    fname = '.'.join([start, '{}_probs'.format(clf_name), end])
    write_data(fname, kgp_data)

def calculate_benign_threshold(clf, X, Y):
    """
    Calculate a decision boundary in order to ensure that we have PATHOGENIC_THRESHOLD
    accuracy on the hgmd_test set.

    clf: A trained classifier which has the 'predict_proba' function.
    """
    hgmd_test = X[np.where(Y == 1)[0]]
    hgmd_test_labels = Y[np.where(Y == 1)[0]]

    kgp_test = X[np.where(Y == 0)[0]]
    baylor_test_labels = Y[np.where(Y == 0)[0]]

    thresholds = np.arange(0, 1.01, .01)
    best_threshold = 0.0
    best_hgmd_acc = 0.0
    best_baylor_acc = 0.0

    hgmd_probs = clf.predict_proba(hgmd_test)
    baylor_probs = clf.predict_proba(kgp_test)
    
    for threshold in thresholds:
        hgmd_preds = (hgmd_probs > threshold).astype('float')[:,1]
        hgmd_acc = accuracy_score(hgmd_test_labels.flatten(), hgmd_preds)

        baylor_preds = (baylor_probs > threshold).astype('float')[:,1]
        baylor_acc = accuracy_score(baylor_test_labels.flatten(), baylor_preds)

        if baylor_acc > best_baylor_acc and hgmd_acc >= PATHOGENIC_THRESHOLD:
            best_threshold = threshold
            best_hgmd_acc = hgmd_acc
            best_baylor_acc = baylor_acc

    return (best_threshold, best_hgmd_acc, best_baylor_acc)

def calculate_pathogenic_threshold(clf, X, Y):
    """
    Calculate a decision boundary in order to ensure that we have BENIGN_THRESHOLD
    accuracy on the benign_test set.

    clf: A trained classifier which has the 'predict_proba' function.
    """
    hgmd_test = X[np.where(Y == 1)[0]]
    hgmd_test_labels = Y[np.where(Y == 1)[0]]

    kgp_test = X[np.where(Y == 0)[0]]
    baylor_test_labels = Y[np.where(Y == 0)[0]]

    thresholds = np.arange(0, 1.01, .01)
    best_threshold = 0.0
    best_hgmd_acc = 0.0
    best_baylor_acc = 0.0

    hgmd_probs = clf.predict_proba(hgmd_test)
    baylor_probs = clf.predict_proba(kgp_test)
    
    for threshold in thresholds:
        hgmd_preds = (hgmd_probs > threshold).astype('float')[:,1]
        hgmd_acc = accuracy_score(hgmd_test_labels.flatten(), hgmd_preds)

        baylor_preds = (baylor_probs > threshold).astype('float')[:,1]
        baylor_acc = accuracy_score(baylor_test_labels.flatten(), baylor_preds)

        if baylor_acc >= BENIGN_THRESHOLD and hgmd_acc > best_hgmd_acc:
            best_threshold = threshold
            best_hgmd_acc = hgmd_acc
            best_baylor_acc = baylor_acc

    return (best_threshold, best_hgmd_acc, best_baylor_acc)

def evaluate(clf, processor, with_metrics=False, only_metrics=False, save_probs=False, return_clfs=False):
    """
    Evaluate the clf with 5-fold cross validation.

    clf: a classifier which has 'fit' and 'predict_proba' functions
    processor: a function which preprocesses the data before fitting and testing
    """
    X, Y = get_dataset(with_metrics, only_metrics=only_metrics)
    pathogenic_data_with_probs = []
    benign_data_with_probs = []
    benign_results = []
    pathogenic_results = []
    trained_clfs = []

    skf = StratifiedKFold(Y.flatten(), n_folds=NUM_FOLDS, shuffle=True)
    for train_idxs, test_idxs in skf:
        X_train, X_test = X[train_idxs], X[test_idxs]
        Y_train, Y_test = Y[train_idxs], Y[test_idxs]

        X_train, X_test, _ = processor(X_train, X_test, np.array([X_train[0]]))

        # Get a new copy of the clf that hasn't been fitted yet
        clf = sklearn.base.clone(clf)

        # Fit the classifier and test on the held out fold
        clf.fit(X_train, Y_train.flatten())
        benign_results.append(calculate_benign_threshold(clf, X_test, Y_test))
        pathogenic_results.append(calculate_pathogenic_threshold(clf, X_test, Y_test))
        print('Benign threshold:', benign_results[-1])
        print('Pathogenic threshold:', pathogenic_results[-1])

        # Add the classifier probabilities for the held out set to the data matrix
        if save_probs:
            pathogenic_probs = clf.predict_proba(X_test[np.where(Y_test == 1)[0]])
            pathogenic_data_with_probs.append(np.hstack((X_test[np.where(Y_test == 1)[0]], pathgogenic_probs[:,1:])))
            benign_probs = clf.predict_proba(X_test[np.where(Y_test == 0)[0]])
            benign_data_with_probs.append(np.hstack((X_test[np.where(Y == 0)[0]], benign_probs[:,1:])))

        # Keep this clf to return later
        if return_clfs:
            trained_clfs.append(clf)

    if save_probs:
        pathogenic_data_with_probs = np.vstack(pathogenic_data_with_probs)
        benign_data_with_probs = np.vstack(benign_data_with_probs)
        write_probs(clf.__class__.__name__, pathogenic_data_with_probs, benign_data_with_probs)

    means = np.mean(benign_results, axis=0)
    stds = np.std(benign_results, axis=0)
    print('Benign threshold stats:')
    print('benign threshold:\t{:.5f} +/- {:.5f}'.format(means[0], stds[0]))
    print('benign hgmd_acc:\t{:.5f} +/- {:.5f}'.format(means[1], stds[1]))
    print('benign baylor_acc:\t{:.5f} +/- {:.5f}\n'.format(means[2], stds[2]))

    means = np.mean(pathogenic_results, axis=0)
    stds = np.std(pathogenic_results, axis=0)
    print('Pathogenic threshold stats:')
    print('pathogenic threshold:\t{:.5f} +/- {:.5f}'.format(means[0], stds[0]))
    print('pathogenic hgmd_acc:\t{:.5f} +/- {:.5f}'.format(means[1], stds[1]))
    print('pathogenic baylor_acc:\t{:.5f} +/- {:.5f}\n'.format(means[2], stds[2]))

    if return_clfs:
        return trained_clfs
