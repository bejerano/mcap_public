import argparse

from sklearn.externals import joblib
from sklearn.metrics import roc_curve

from utils import get_dataset
from utils import cadd_scores
from utils import polyphen2_scores
from utils import sift_scores
from utils import dbnsfp_svm_scores
from utils import dbnsfp_lr_scores
from utils import eigen_scores

from calculate_five_fold_curves import calculate_auc_from_scores
from calculate_five_fold_curves import calculate_tnr_fnr

from plot_curves import plot_roc
from plot_curves import plot_fnr_fpr
from plot_curves import plot_zoom_roc


PATHOGENIC_TEST = "<FILEPATH to PATHOGENIC TEST DATA>"
BENIGN_TEST     = "<FILEPATH to BENIGN TEST DATA>"

def calculate_results(clf, X, Y):
    results = {}
    probs = clf.predict_proba(X)
    fpr, tpr, thresholds = roc_curve(Y, probs[:,1])

    results['AUC'] = {}
    results['AUC']['mean_tpr'] = tpr
    results['AUC']['mean_fpr'] = fpr

    tnr, fnr = calculate_tnr_fnr(probs[:,1], Y)
    results['FNR'] = {}
    results['FNR']['mean_tnr'] = tnr
    results['FNR']['mean_fnr'] = fnr

    return results

def calculate_our_results(clf_with_metrics):
    results = {}

    X, Y = get_dataset(True, PATHOGENIC_TEST, BENIGN_TEST)
    results['M-CAP'] = calculate_results(clf_with_metrics, X, Y)

    return results

def add_other_results(results):
    scores = [
        ('CADD', cadd_scores),
        ('PolyPhen-2', polyphen2_scores),
        ('SIFT', sift_scores),
        ('MetaLR', dbnsfp_lr_scores),
        ('Eigen', eigen_scores),
    ]

    for name, scores_func in scores:
        X, Y = scores_func(PATHOGENIC_TEST, BENIGN_TEST)
        tpr, fpr = calculate_auc_from_scores(X, Y)
        results[name] = {}
        results[name]['AUC'] = {}
        results[name]['AUC']['mean_tpr'] = tpr
        results[name]['AUC']['mean_fpr'] = fpr

        tnr, fnr = calculate_tnr_fnr(X, Y)
        results[name]['FNR'] = {}
        results[name]['FNR']['mean_tnr'] = tnr
        results[name]['FNR']['mean_fnr'] = fnr

    return results

def plot_test_curves(clf_with_metrics):
    clf_with_metrics = joblib.load(clf_with_metrics)

    results = calculate_our_results(clf_with_metrics)
    results = add_other_results(results)

    plot_roc(results, 'testset_roc.pdf')
    plot_fnr_fpr(results, 'testset_fnr.pdf')
    plot_zoom_roc(results, 'testset_zoom_roc.pdf')

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('clf_with_metrics', type=str,
        help='Saved clf which uses other metrics')

    args = ap.parse_args()

    plot_test_curves(args.clf_with_metrics)
