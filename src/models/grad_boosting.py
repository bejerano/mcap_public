from __future__ import print_function

import argparse
import datetime
import getpass
import os
import sys

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib

from rf import rf_processor
from utils import evaluate
from utils import get_dataset
import numpy as np

# default set of parameters if none are input.
LR = 0.1
N_ESTIMATORS = 700
MAX_DEPTH = 3
MIN_LEAVES = 2
SUBSAMPLE = 1.0

RESULTS_DIR = '/cluster/u/{}/pathogenicity_classification/results/trained_models/grad_boosted/'
RESULTS_DIR = RESULTS_DIR.format(getpass.getuser())

def fit_model(clf, with_metrics, only_metrics=False):
    X, Y = get_dataset(with_metrics, only_metrics=only_metrics)
    pathogenic_indices = np.ones(len(Y))
    pathogenic_indices[np.where(Y.flatten()==1)] = 10
    X = np.repeat(X, pathogenic_indices.astype(int), axis=0)
    Y = np.repeat(Y, pathogenic_indices.astype(int), axis=0)

    clf.fit(X, Y.flatten())
    return clf

def save_clf(clf, results_dir):
    fname = os.path.join(results_dir, 'clf.pkl')
    joblib.dump(clf, fname)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('--n-estimators', type=int, default=N_ESTIMATORS,
        help='Number of Gradient Boosting estimators')
    ap.add_argument('--lr', type=float, default=LR, help='Learning rate')
    ap.add_argument('--max-depth', type=int, default=MAX_DEPTH,
    	help='The maximum depth of the individual regression estimators')
    ap.add_argument('--subsample', type=float, default=SUBSAMPLE,
    	help='The fraction of samples to be used for fitting the individual estimators')
    ap.add_argument('--max-features', nargs='?', const=None, type=int,
    	help='Number of features to consider when looking for the best split')
    ap.add_argument('--min-leaves', type=int, default=MIN_LEAVES,
    	help='Minimum number of samples required to be at a leaf node')
    ap.add_argument('--with-metrics', action='store_true')
    ap.add_argument('--only-metrics', action='store_true')
    ap.add_argument('--v', nargs='?', const=1, default=0, help='Enable verbose output')
    ap.add_argument('--out', type=str, default=None, help='Log output to given file')
    ap.add_argument('--save-clf', action='store_true',
        help='Train the model on all of the data and store the clf for testing.')
    args = ap.parse_args()

    if args.save_clf:
        dirname = datetime.datetime.now().strftime("%d_%m_%Y_%H:%M:%S")
        results_dir = os.path.join(RESULTS_DIR, dirname)
        os.makedirs(results_dir)
        params_fname = os.path.join(results_dir, 'params.txt')
        args.out = params_fname

    if args.out:
        sys.stdout = open(args.out, 'w')

    print('n_estimators:\t{}'.format(args.n_estimators))
    print('lr:\t\t{}'.format(args.lr))
    print('subsample:\t{}'.format(args.subsample))
    print('max_depth:\t{}'.format(args.max_depth))
    print('max_features:\t{}'.format(args.max_features))
    print('min_leaves:\t{}'.format(args.min_leaves))
    clf = GradientBoostingClassifier(
    	n_estimators=args.n_estimators,
    	learning_rate=args.lr,
        subsample=args.subsample,
    	max_depth=args.max_depth,
    	max_features=args.max_features,
    	min_samples_leaf=args.min_leaves,
        verbose=args.v,
    )

    if args.save_clf:
        clf = fit_model(clf, args.with_metrics, only_metrics=args.only_metrics)
        save_clf(clf, results_dir)
    else:
        evaluate(clf, rf_processor, with_metrics=args.with_metrics, only_metrics=args.only_metrics)

    if args.out:
        sys.stdout.close()
