import argparse
import getpass
import numpy as np
import os
import time

from sklearn.ensemble import RandomForestClassifier

from utils import evaluate
from utils import get_feature_names

FEATURE_IMPORTANCES = '/cluster/u/{}/pathogenicity_classification/results/rf_importances_{}.txt'
FEATURE_IMPORTANCES = FEATURE_IMPORTANCES.format(getpass.getuser(), time.strftime("%Y%m%d-%H%M%S"))
N_ESTIMATORS = 700

def rf_processor(X_train, hgmd_test, kgp_test):
    return X_train, hgmd_test, kgp_test

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('--n-estimators', type=int, default=N_ESTIMATORS,
        help='Number of Random Forest estimators')
    ap.add_argument('--save-importances', action='store_true',
        help='Save Random Forest feature importances')
    args = ap.parse_args()

    rf = RandomForestClassifier(n_estimators=args.n_estimators)
    trained_rfs = evaluate(rf, rf_processor, return_clfs=True)

    if args.save_importances:
        importances = [rf.feature_importances_ for rf in trained_rfs]
        labels = get_feature_names()

        directory = os.path.dirname(FEATURE_IMPORTANCES)
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(FEATURE_IMPORTANCES, 'w') as f:
    	    header = '\t'.join(['Name'] + labels)
    	    f.write(header)
    	    for i in range(len(importances)):
    		    line = ['rf_{}'.format(i)] + [str(n) for n in importances[i]]
    		    f.write('\n' + '\t'.join(line))

        print('Saved feature importances to {}'.format(FEATURE_IMPORTANCES))
